﻿
{*****************************************************************************}
{                                                                             }
{                 _   _                  _____                                }
{                | \ | |                / ____|                               }
{                |  \| |   __ _   ____ | |        ___                         }
{                | . ` |  / _` | |_  / | |       / _ \                        }
{                | |\  | | (_| |  / /  | |____  | (_) |                       }
{                |_| \_|  \__,_| /___|  \_____|  \___/                        }
{                                                                             }
{                                                                             }
{                                                                             }
{   Author    : Nazar Chaikovskyi <nazar1986@gmail.com>                       }
{                                                                             }
{                 Copyright(c) 2016 Nazar Chaikovskyi                         }
{                                                                             }
{*****************************************************************************}



unit nazco.Splash;

interface

uses
  Windows, PNGImage, Forms, Graphics, SysUtils, Messages, ExtCtrls, Classes,
  StdCtrls, Vcl.Controls, Dialogs, Generics.Collections;


type
  TSplashFormClass = class of TSplashFormBase;

  {$REGION 'TSplashFormBase'}
  TSplashFormBase = class(TForm)
  private
    FDC           : HDC;
    FOutBitmap    : TBitmap;

    FBaseImage    : TPngImage;
    FOutImage     : TPngImage;

    FSplashText   : String;
    FUserCanClose : Boolean;
    FTextRectList : TDictionary<TRect, TNotifyEvent>;

    procedure OnVersionLabelClick(Sender: TObject);

    function  IsMouseCursorOverControl(AExecAction: Boolean = False): Boolean;

    procedure CreateBitmapFromPng(ADstBitMap: TBitmap; ASrcPngImage: TPngImage);

    procedure RenderForm(AAlpha: Byte = 255);
  protected
    procedure WndProc(var AMessage: TMessage); override;
    procedure DoDrawTextOnSplash(AText: String; ALeft, ATop, AFontSize: Integer; AColor: TColor; AStyle: TFontStyles; AOnClick: TNotifyEvent = nil);

    procedure DoLoadSplashImage(); virtual;
    procedure DoDrawSplashControls(); virtual;
    procedure DoDrawSplash();
  public
    constructor CreateNew(AOwner: TComponent; Dummy: Integer  = 0); override;
    destructor  Destroy(); override;

    procedure CloseSplash();
    procedure DrawSplash(AText: String);
  end;
  {$ENDREGION}

  {$REGION 'TSplash'}
  TAppSplash = class
  private
    FSplashForm   : TSplashFormBase;
    FHideTimer    : TTimer;

    procedure HideTimer(Sender: TObject);
  protected
    constructor Create(AUserCanClose: Boolean);
  public
    destructor Destroy(); override;

    class procedure ShowSplash(AUserCanClose: Boolean = False; AText: String = '');
    class procedure RefreshSplash(AText: String = '');
    class procedure HideSplash();
  end;
  {$ENDREGION}

implementation

const
  cvVersion = 'Version x.x.x.x';
var
  AppSplash : TAppSplash = nil;


{$REGION 'TSplashFormBase implementation'}

//---------------------------------------------------------------------------
procedure TSplashFormBase.CloseSplash();
begin
  if (FUserCanClose) then
    TAppSplash.HideSplash();
end;

//---------------------------------------------------------------------------
constructor TSplashFormBase.CreateNew(AOwner: TComponent; Dummy: Integer);
begin
  inherited;

  FDC := GetDC(0);

  FBaseImage := TPngImage.Create();
  FOutImage  := TPngImage.Create();
  DoLoadSplashImage();

  FOutBitmap := TBitmap.Create();
  FOutBitmap.PixelFormat:=pf32bit;
  FOutBitmap.Width := FBaseImage.Width;
  FOutBitmap.Height := FBaseImage.Height;


  FSplashText := '';

  Self.BorderStyle := bsNone;
  Self.Width := FBaseImage.Width;
  Self.Height := FBaseImage.Height;
  Self.Position := poScreenCenter;
  Self.DoubleBuffered := True;

  FTextRectList := TDictionary<TRect, TNotifyEvent>.Create();
end;

//---------------------------------------------------------------------------
destructor TSplashFormBase.Destroy();
begin
  FreeAndNil(FTextRectList);
  FreeAndNil(FBaseImage);
  FreeAndNil(FOutImage);

  FreeAndNil(FOutBitmap);
  ReleaseDC(0, FDC);

  inherited;
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.DoDrawSplashControls();
begin
  DoDrawTextOnSplash(FSplashText, {Self.Left +} 100, Self.Height -100, 18, clBlack, [fsBold], OnVersionLabelClick);
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.DoDrawTextOnSplash(AText: String; ALeft, ATop, AFontSize: Integer; AColor: TColor; AStyle: TFontStyles; AOnClick: TNotifyEvent);
begin
  if (not AText.Trim.IsEmpty) then
  begin
    SetBkMode(FOutImage.Canvas.Handle, TRANSPARENT);
    FOutImage.Canvas.Font.Size := AFontSize;
    FOutImage.Canvas.Font.Color := AColor;
    FOutImage.Canvas.Font.Style := AStyle;
    FOutImage.Canvas.TextOut(ALeft, ATop, AText);

    if (Assigned(AOnClick)) then
      FTextRectList.Add( TRect.Create(ALeft, ATop, ALeft + FOutImage.Canvas.TextWidth(AText), ATop + FOutImage.Canvas.TextHeight(AText)), AOnClick );
  end;
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.DoLoadSplashImage();
begin
  FBaseImage.LoadFromResourceName(HInstance, 'splash_image1');
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.DrawSplash(AText: String);
begin
  FTextRectList.Clear();
  FSplashText := AText;

  DoDrawSplash();

  if (not Self.Showing) then
    Self.Show();
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.DoDrawSplash();
begin
  FOutImage.Assign(FBaseImage);

  DoDrawSplashControls();
  CreateBitmapFromPng(FOutBitmap, FOutImage);

  RenderForm();
end;

//---------------------------------------------------------------------------
function TSplashFormBase.IsMouseCursorOverControl(AExecAction: Boolean): Boolean;
var
  vMyPoint  : TPoint;
  vPair     : TPair<TRect, TNotifyEvent>;
begin
  Result := False;
  try
    vMyPoint := Self.ScreenToClient(Mouse.CursorPos);

    for vPair in FTextRectList do
    begin
      Result := (PtInRect(vPair.Key, vMyPoint));
      if (Result) then
      begin
        if (AExecAction) then
          vPair.Value(nil);
        Break;
      end;
    end;
  except
    Result := False;
  end;
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.OnVersionLabelClick(Sender: TObject);
begin
  ShowMessage(cvVersion);
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.RenderForm(AAlpha: Byte);
var
  vBitmapSize : TSize;
  vBitmapPos  : TPoint;
  vTopLeft    : TPoint;
  vBlendFunc  : TBlendFunction;
begin
  SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle, GWL_EXSTYLE) or WS_EX_LAYERED);

  Self.Width   := FOutBitmap.Width;
  Self.Height  := FOutBitmap.Height;

  //* розмір BitMap
  vBitmapSize.cx := FOutBitmap.Width;
  vBitmapSize.cy := FOutBitmap.Height;
  //* координати BitMap
  vBitmapPos := Point(0,0);

  vBlendFunc.BlendOp := AC_SRC_OVER;
  vBlendFunc.BlendFlags := 0;
  vBlendFunc.AlphaFormat := AC_SRC_ALPHA;
  vBlendFunc.SourceConstantAlpha := AAlpha;

  vTopLeft := BoundsRect.TopLeft;

  UpdateLayeredWindow(
      Self.Handle,
      FDC,
      @vTopLeft,
      @vBitmapSize,
      FOutBitmap.Canvas.Handle,
      @vBitmapPos,
      0,
      @vBlendFunc,
      ULW_ALPHA);
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.CreateBitmapFromPng(ADstBitMap: TBitmap; ASrcPngImage: TPngImage);
type
  TRGBTripleArray = ARRAY[Word] of TRGBTriple;
  pRGBTripleArray = ^TRGBTripleArray;
  TRGBAArray = array[Word] of TRGBQuad;
  PRGBAArray = ^TRGBAArray;
var
  x, y          : Integer;
  vTripleAlpha  : Double;
  pColor        : pRGBTripleArray;
  pAlpha        : pngimage.pbytearray;
  pBmp          : pRGBAArray;
begin
  ADstBitMap.Height := ASrcPngImage.Height;
  ADstBitMap.Width := ASrcPngImage.Width;
  ADstBitMap.PixelFormat := pf32bit;

  for y := 0 to ASrcPngImage.Height - 1 do
  begin
    pAlpha := ASrcPngImage.AlphaScanline[y];
    pColor := ASrcPngImage.Scanline[y];
    pBmp := ADstBitMap.ScanLine[y];

    for x := 0 to ASrcPngImage.Width - 1 do
    begin
      pBmp[x].rgbReserved := pAlpha[x];

      vTripleAlpha := pBmp[x].rgbReserved / 255;
      pBmp[x].rgbBlue := byte(trunc(pColor[x].rgbtBlue * vTripleAlpha));
      pBmp[x].rgbGreen := byte(trunc(pColor[x].rgbtGreen * vTripleAlpha));
      pBmp[x].rgbRed := byte(trunc(pColor[x].rgbtRed * vTripleAlpha));
    end;
  end;
end;

//---------------------------------------------------------------------------
procedure TSplashFormBase.WndProc(var AMessage: TMessage);
begin
  inherited;

  case AMessage.Msg of
    WM_MOUSEMOVE:
        begin
          if (IsMouseCursorOverControl()) then
            Cursor := crHandPoint
          else
            Cursor := crDefault;
        end;
    WM_LBUTTONDOWN:
        begin
          if (not IsMouseCursorOverControl(True)) then
            CloseSplash();
        end;

    WM_KEYDOWN:
        begin
          if (AMessage.WParam in [VK_ESCAPE, VK_RETURN]) then
            CloseSplash();
        end;
  end;
end;
{$ENDREGION}

{$REGION 'TSplash implementation'}
//---------------------------------------------------------------------------
constructor TAppSplash.Create(AUserCanClose: Boolean);
begin
  inherited Create();

  FSplashForm := TSplashFormClass.CreateNew(nil);
  FSplashForm.FUserCanClose := AUserCanClose;

  FHideTimer := TTimer.Create(nil);
  FHideTimer.Interval := 5 * 1000;
  FHideTimer.OnTimer := HideTimer;
  FHideTimer.Enabled := True;
end;

//---------------------------------------------------------------------------
destructor TAppSplash.Destroy();
begin
  FreeAndNil(FHideTimer);
  FreeAndNil(FSplashForm);
  inherited;
end;

//---------------------------------------------------------------------------
class procedure TAppSplash.HideSplash();
begin
  if (Assigned(AppSplash)) then
    FreeAndNil(AppSplash);
end;

//---------------------------------------------------------------------------
procedure TAppSplash.HideTimer(Sender: TObject);
begin
  FHideTimer.Enabled := False;
  FSplashForm.CloseSplash();
end;

//---------------------------------------------------------------------------
class procedure TAppSplash.RefreshSplash(AText: String);
begin
  if (Assigned(AppSplash)) then
    AppSplash.FSplashForm.DrawSplash(AText);
end;

//---------------------------------------------------------------------------
class procedure TAppSplash.ShowSplash(AUserCanClose: Boolean = False; AText: String = '');
begin
  if (not Assigned(AppSplash)) then
  begin
    AppSplash := TAppSplash.Create(AUserCanClose);
    AppSplash.FSplashForm.DrawSplash(AText);
  end;
end;
{$ENDREGION}


end.
